BIN=./bin/greedy
# BIN=./bin/welshpowell
# BIN=./bin/bipartito
DEBUG=1

TODOS=tests/todos/*
BIPARTITOS=tests/bipartitos/*
GRANDES=tests/grandes/*

# check () {
#     for f in $@; do
#         if [ $DEBUG -eq 1 ]; then
#             echo $f
#         fi
#         $BIN < $f > output.txt
#         python3 checker.py
#         if [ $DEBUG -eq 1 ]; then
#             echo
#         fi
#     done
# }
#
# check $BIPARTITOS
# check $TODOS
# check $GRANDES

check_bip () {
    for f in $@; do
        if [ $DEBUG -eq 1 ]; then
            echo $f
        fi
        $BIN < $f
        echo
    done
}

# check_bip $BIPARTITOS
# check_bip $TODOS
check_bip tests/grandes/BxB1100_999_54_2017 tests/grandes/GRD99704280

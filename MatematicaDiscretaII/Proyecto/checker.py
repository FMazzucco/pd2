def ChicoGrandeBC(lines):
    cardinales = []
    prev_col = 2**32
    cant = 0
    for x in lines:
        if x[0:4] == "NODO":
            col = int(x[-8:-1])
            if col == prev_col:
                cant += 1
            else:
                cardinales.append(cant)
                cant = 1
                prev_col = col
    cardinales.append(cant)

    # print(cardinales)
    is_sorted = True
    for x in range(len(cardinales)-1):
        if cardinales[x] > cardinales[x+1]:
            is_sorted = False
            print(x)
    print("is sorted?", is_sorted)

def WelshPowell(lines):
    grados = []
    for x in lines:
        if x[0:4] == "NODO":
            grado = int(x.split(",")[1][-5:])
            grados.append(grado)

    # print(grados)
    is_sorted = True
    for x in range(len(grados)-1):
        if grados[x] < grados[x+1]:
            is_sorted = False
            break
    print("is sorted?", is_sorted)

def RevierteBC(lines):
    colores = []
    prev_col = 2**32
    for x in lines:
        if x[0:4] == "NODO":
            col = int(x[-8:-1])
            if col != prev_col:
                colores.append(col)
                # print(col)
                prev_col = col

    is_sorted = True
    for x in range(len(colores)-1):
        if colores[x] < colores[x+1]:
            is_sorted = False
            break
            # print(x, colores[x-1], colores[x], colores[x+1])
    print("is sorted? ", is_sorted)

def Greedy(lines):
    cant = 0
    colores = []
    prev_col = 2**32
    for x in lines:
        if x[0:4] == "CANT":
            cant = int(x[11:-1])
            # print("CANTIDAD",cant)

        elif x[0:4] == "NODO":
            col = int(x[-6:-1])
            if col != prev_col:
                colores.append(col)
                prev_col = col

    all_colors = True
    max = colores[0]
    print("           ",max+1==cant,max, cant)
    for x in range(max):
        if x not  in colores:
            all_colors = False
            break
    print("all_colors?",all_colors)

a = open("output.txt","r")
lines = a.readlines()
a.close()

# WelshPowell(lines)
# RevierteBC(lines)
# ChicoGrandeBC(lines)
# Greedy(lines)
